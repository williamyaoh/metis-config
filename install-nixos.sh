set -e

NIXOS_ROOT=/mnt

if [ -n "$1" ]; then
  NIXOS_ROOT=$1
fi

# Strip a trailing slash, if it exists.
NIXOS_ROOT=${NIXOS_ROOT%/}

echo "installing NixOS under filesystem at $NIXOS_ROOT..."

# We assume that you've already set up the partitions using `parted' or friends,
# and created filesystems on them. Doing this correctly, programatically, is
# a little bit too hard.

swapon /dev/disk/by-partlabel/Swap
mount /dev/disk/by-partlabel/NixOS $NIXOS_ROOT

# Set up NixOS configuration. Copy the current directory into `/home/nixos/'.
NIXOS_CONFIG_ROOT=${NIXOS_ROOT}/home/nixos

mkdir -p ${NIXOS_ROOT}/home
mount /dev/disk/by-partlabel/Cram ${NIXOS_ROOT}/home
cp -r $(dirname $0) $NIXOS_CONFIG_ROOT
chown -R 0:7777 $NIXOS_CONFIG_ROOT
chmod -R 775 $NIXOS_CONFIG_ROOT
find $NIXOS_CONFIG_ROOT -type f -exec chmod 664 '{}' ';'

mkdir -p ${NIXOS_ROOT}/etc
# This symbolic link has to be relative because on our first install,
# we've typically got the root filesystem mounted under `/mnt'.
# Thankfully, the toplevel directories are always gonna look the same.
ln -s "../home/nixos" ${NIXOS_ROOT}/etc/nixos

# We assume that wireless is desired. Remove this part if it's not.
echo "Setting up wireless..."
read -p "Enter name of WPA configuration file (blank for '/root/wpa_supplicant.conf'): " WPA_CONFIGURATION

if [ -z "$WPA_CONFIGURATION" ]; then
  WPA_CONFIGURATION="/root/wpa_supplicant.conf"
fi

cp "$WPA_CONFIGURATION" ${NIXOS_ROOT}/etc/wpa_supplicant.conf

nixos-install --root $NIXOS_ROOT

reboot
