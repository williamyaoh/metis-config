# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking = {
    hostName = "empanda-nixos";
    firewall = {
      allowedTCPPorts = [ 22222 ];
    };
  };

  i18n = {
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
    inputMethod = {
      enabled = "ibus";
      ibus.engines = with pkgs.ibus-engines; [ mozc ];
    };
  };

  fonts = {
    enableDefaultFonts = true;
    enableFontDir = true;

    fontconfig.enable = true;
    fontconfig.antialias = true;

    fonts = with pkgs; [
      ubuntu_font_family
      source-code-pro
      fira-code
      fira-code-symbols
      noto-fonts
      noto-fonts-cjk
    ];
  };

  time.timeZone = "America/Chicago";

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    allowSFTP = true;
    challengeResponseAuthentication = true;
    forwardX11 = true;
    ports = [ 22222 ];
  };

  # Enable a local PG server.
  services.postgresql.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable OpenVPN on startup.
  # services.openvpn.servers = {
  #   workVPN = { config = '' config /etc/nixos/client.ovpn ''; };
  # };

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us";

    displayManager.lightdm.enable = true;

    windowManager.xmonad = {
      enable = true;
      haskellPackages = pkgs.haskellPackages;
      enableContribAndExtras = true;
      extraPackages = haskellPackages:
        [ haskellPackages.xmonad-contrib ];
    };

    desktopManager.plasma5 = {
      enable = true;
      enableQt4Support = false;
    };

    videoDrivers = [ "nvidia" ];
  };

  hardware.opengl.driSupport32Bit = true;

  virtualisation.docker = {
    enable = true;
    enableOnBoot = true;
  };

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  environment.systemPackages = with pkgs; [
    firefoxWrapper

    wget
    curl

    emacs26
    terminator

    unzip
    ripgrep

    gcc
  ];

  documentation = {
    man.enable = true;
  };

  users.extraUsers.william = {
    isNormalUser = true;
    home = "/home/william";
    description = "William Yao";
    extraGroups =
      [ "wheel"
        "networkmanager"
        "dialout"
        "docker"
      ];
  };

  users.enforceIdUniqueness = true;  # also applies to groups

  nix = {
    useSandbox = true;
    readOnlyStore = true;
  };

  nixpkgs.config.allowUnfree = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03";
}
